package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by maryb on 14.06.2017.
 */
public class StringComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return (-1)*o1.compareTo(o2);
    }
}
