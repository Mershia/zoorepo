package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }

    @Override
    public String toString() {
        return "Lizard [scalesColor=" + scalesColor + super.toString() + "]";
    }

    @Override
    public int hashCode() {
        return 51* getName().hashCode() & 37 * getScalesColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {    // czy referencje sie zgadzaja, adres w pamieci
            return true;
        }
        if (!(obj instanceof Lizard)) {
            return false;
        }

        Lizard lizard = (Lizard) obj;   // rzutowanie, bo wiemy ze jest tej instancji, bo nie wyrzucil false;
        return getName().equals(lizard.getName()) && getScalesColor().equals(lizard.getScalesColor());
    }

    @Override
    public int compareTo(Animal o) {   // jesli jest ssakiem to wtedy porownujemy futro

        int ret = super.compareTo(o);
        if(ret ==0){
            if (o instanceof Lizard){   // czy ma futro?
                return scalesColor.compareTo(((Lizard)o).getScalesColor());
            }
        }
        return ret;
    }
}
