package pl.codementors.zoo.animals;

/**
 * Just an animal.
 *
 * @author psysiu
 */
public abstract class Animal implements Comparable<Animal> {

    /**
     * Animal name.
     */
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal [name = " + name + "]";
    }

    // porownujemy wedlug imienia         jesli dostaniemy dwa takie same ssaki to przechodzimy do mamal
    @Override
    public int compareTo(Animal o) {
        int ret = name.compareTo(o.getName());
        if(ret==0){
            return getClass().getCanonicalName().compareTo(o.getClass().getCanonicalName());
        }
        return name.compareTo(o.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {    // czy referencje sie zgadzaja, adres w pamieci
            return true;
        }
        if (!(obj instanceof Animal)) {
            return false;
        }

        Animal animal = (Animal) obj;   // rzutowanie, bo wiemy ze jest tej instancji, bo nie wyrzucil false;
        return getName().equals(animal.getName());
    }

    @Override
    public int hashCode() {
        return 37 * getName().hashCode();
    }
}
