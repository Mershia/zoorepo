package pl.codementors.zoo.animals;

/**
 * Created by psysiu on 6/13/17.
 */
public class Tiger extends Mammal {

    public Tiger(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        //return "Tiger [name=" + getName() + "furColor=" + getFurColor()+ "]";
        return "Tiger [" + super.toString() + "]";
    }

    @Override
    public int hashCode() {
        return 51* getName().hashCode() & 37* getFurColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {    // czy referencje sie zgadzaja, adres w pamieci
            return true;
        }
        if (!(obj instanceof Tiger)) {
            return false;
        }

        Tiger tiger = (Tiger) obj;   // rzutowanie, bo wiemy ze jest tej instancji, bo nie wyrzucil false;
        return getName().equals(tiger.getName()) && getFurColor().equals(tiger.getFurColor());
    }


//    @Override
//    public int compareTo(Animal animal) {
//            if(animal instanceof Tiger){
//                return super.compareTo(animal);
//            }else{
//                int ret = super.
//            }
//        return getFurColor().compareTo(((Tiger)animal).getFurColor());
    }

