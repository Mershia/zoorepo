package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by maryb on 13.06.2017.
 */
public class AnimalComparator implements Comparator<Animal> {

    @Override
    public int compare(Animal o1, Animal o2) {
        String class1 = o1.getClass().getSimpleName();
        String class2 = o2.getClass().getSimpleName();

        // poniewaz sa Stringami maja dobra metode compareTo
        int ret = class1.compareTo(class2);

        if(ret == 0){
            ret = o1.getName().compareTo(o2.getName());
        }
        return ret;
    }
}
