package pl.codementors.zoo.animals;

/**
 * Just a mammal with some fur.
 *
 * @author psysiu
 */
public class Mammal extends Animal {

    /**
     * Color of the animal fur.
     */
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    @Override
    public String toString() {
        return "Mammal [furColor=" + furColor + super.toString() + "]";
    }

    // return "Mammal [furColor= " + furColor + " name =" + get


    @Override
    public int hashCode() {
        return 51* getName().hashCode() & 37 * getFurColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {    // czy referencje sie zgadzaja, adres w pamieci
            return true;
        }
        if (!(obj instanceof Mammal)) {
            return false;
        }

        Mammal mammal = (Mammal) obj;   // rzutowanie, bo wiemy ze jest tej instancji, bo nie wyrzucil false;
        return getName().equals(mammal.getName()) && getFurColor().equals(mammal.getFurColor());
    }

    @Override
    public int compareTo(Animal o) {   // jesli jest ssakiem to wtedy porownujemy futro

        int ret = super.compareTo(o);
        if(ret ==0){
            if (o instanceof Mammal){   // czy ma futro?
                return furColor.compareTo(((Mammal)o).getFurColor());
            }
        }
        return ret;
    }
}
