package pl.codementors.zoo.animals;

/**
 * Just a bird with feathers.
 *
 * @author psysiu
 */
public class Bird extends Animal {

    /**
     * Color of the bird feathers.
     */
    private String featherColor;

    public Bird(String name, String featherColor) {
        super(name);
        this.featherColor = featherColor;
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }

    @Override
    public String toString() {
        return "Bird [featherColor=" + featherColor + super.toString() + "]";
    }

    @Override
    public int hashCode() {
        return 51* getName().hashCode() & 37* getFeatherColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {    // czy referencje sie zgadzaja, adres w pamieci
            return true;
        }
        if (!(obj instanceof Bird)) {
            return false;
        }

        Bird bird = (Bird) obj;   // rzutowanie, bo wiemy ze jest tej instancji, bo nie wyrzucil false;
        return getName().equals(bird.getName()) && getFeatherColor().equals(bird.getFeatherColor());
    }

//    @Override
//    public int compareTo(Animal o) {
//        return getName().compareTo(o.getName()) & getFeatherColor().compareTo(o.);
//    }

    @Override
    public int compareTo(Animal o) {   // jesli jest ssakiem to wtedy porownujemy futro

        int ret = super.compareTo(o);
        if(ret ==0){
            if (o instanceof Bird){   // czy ma futro?
                return featherColor.compareTo(((Bird)o).getFeatherColor());
            }
        }
        return ret;
    }
}
