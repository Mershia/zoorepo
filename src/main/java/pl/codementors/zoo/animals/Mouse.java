package pl.codementors.zoo.animals;

/**
 * Created by psysiu on 6/13/17.
 */
public class Mouse extends Mammal {

    public Mouse(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        return "Mouse [" + super.toString() + "]";
    }

    @Override
    public int hashCode() {
        return 51* getName().hashCode() & 38 * getFurColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {    // czy referencje sie zgadzaja, adres w pamieci
            return true;
        }
        if (!(obj instanceof Mouse)) {
            return false;
        }

        Mouse mouse = (Mouse) obj;   // rzutowanie, bo wiemy ze jest tej instancji, bo nie wyrzucil false;
        return getName().equals(mouse.getName()) && getFurColor().equals(mouse.getFurColor());
    }
}
