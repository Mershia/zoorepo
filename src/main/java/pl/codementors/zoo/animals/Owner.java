package pl.codementors.zoo.animals;

/**
 * Created by maryb on 15.06.2017.
 */
public class Owner {

    public enum Type {
        BREEDER,
        COLLECTOR;
    }

    private Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    private String nameOwner;

    public String getNameOwner() {
        return nameOwner;
    }

    public void setNameOwner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    public Owner(){
    }

    public Owner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Owner owner = (Owner) o;

        return nameOwner != null ? nameOwner.equals(owner.nameOwner) : owner.nameOwner == null;
    }

    @Override
    public int hashCode() {
        return nameOwner != null ? nameOwner.hashCode() : 0;
    }
}
