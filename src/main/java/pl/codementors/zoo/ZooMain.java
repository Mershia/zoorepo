package pl.codementors.zoo;

import pl.codementors.zoo.animals.Animal;
import pl.codementors.zoo.animals.AnimalComparator;
import pl.codementors.zoo.animals.Bird;
import pl.codementors.zoo.animals.Lizard;
import pl.codementors.zoo.animals.Mammal;
import pl.codementors.zoo.animals.Mouse;
import pl.codementors.zoo.animals.Owner;
import pl.codementors.zoo.animals.Tiger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();
        Map<Owner, List<Animal>> ownersAndAnimals = new HashMap<>();


        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String ownerName = scanner.next();
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();
                String ownerType = scanner.next();

                Owner.Type typ1 = Owner.Type.valueOf(ownerType);

                Owner owner = new Owner(ownerName);
                owner.setType(typ1);

                if (!ownersAndAnimals.containsKey(owner)){
                    ownersAndAnimals.put(owner, new ArrayList<>());
                }

                switch (type) {
                    case "Bird": {
                        animals.add(new Bird(name, color));
                        ownersAndAnimals.get(owner).add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        animals.add(new Lizard(name, color));
                        ownersAndAnimals.get(owner).add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        animals.add(new Mammal(name, color));
                        ownersAndAnimals.get(owner).add (new Mammal(name, color));
                        break;
                    }

                    case "Tiger": {
                        animals.add(new Tiger(name, color));
                        ownersAndAnimals.get(owner).add(new Tiger(name, color));
                        break;
                    }
                    case "Mouse": {
                        animals.add(new Mouse(name, color));
                        ownersAndAnimals.get(owner).add(new Mouse(name, color));
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }

        print(animals);
        System.out.println("---------*-----------*--------------*--------------");
        Collections.sort(animals, new AnimalComparator());
        print(animals);


        System.out.println("---------*-----------*--------------*--------------");
        System.out.println(animals.hashCode());
        System.out.println("---------*-----------*--------------*--------------");
        Set<Animal> animalSet = new TreeSet<>(new AnimalComparator());
        animalSet.addAll(animals);
        print(animalSet);

        System.out.println("---------*------------*-------------*--------------");
        System.out.println(animalSet.size());

        System.out.println("---------*------------*-------------*--------------");
        Set<Mammal> mammalSet = new HashSet<>();
        List<Bird> birdList = new ArrayList<>();
        Collection<Lizard> lizardCollection = new TreeSet<>();


        for (Animal a : animals) {
            if (a instanceof Mammal) {
                mammalSet.add((Mammal) a);
            } else if (a instanceof Lizard) {
                lizardCollection.add((Lizard) a);
            } else if (a instanceof Bird) {
                birdList.add((Bird) a);
            }

        }
        printAll(mammalSet);
        printAll(lizardCollection);
        printAll(birdList);
        //printAllMapy(animalHashMap);
       // printAllListMapy(animalHashMap);

        Map<String, Animal> animalSetReverse = new HashMap<>();
        //animalSetReverse.putAll(animalHashMap);
        printAllMapy(animalSetReverse);

        System.out.println("---------*------------*-------------*--------------");
        printAllListMapy(ownersAndAnimals);
    }


    public static void print (Collection<Animal> animals){   //można tez 'List' , ale ona jest w tym wypadku za bardzo szczegółowa. A i dziedziczy po kolekcji
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }

    public static void printAll (Collection<? extends Animal> animals){
        for (Animal animal : animals){
            System.out.println(animal);
        }
    }

    public static void printAllMapy(Map<String, Animal> stringAnimalMap){
        for (String key : stringAnimalMap.keySet()) {
            System.out.println(key);
            System.out.println(stringAnimalMap.get(key));
        }
    }

    public static void printAllListMapy(Map<Owner, List<Animal>> stringAnimalMapList){

        for (Owner key : stringAnimalMapList.keySet()) {
            System.out.println(" ");
            System.out.print(key.getNameOwner() + " " + key.getType() + " --> " + " ");


            for(Animal animal : stringAnimalMapList.get(key)){
                System.out.print("[" + animal +"]");
            }
        }
    }
}
